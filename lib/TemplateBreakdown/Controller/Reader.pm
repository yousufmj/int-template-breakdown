package TemplateBreakdown::Controller::Reader;
use Mojo::Base 'Mojolicious::Controller';
use Path::Class qw/dir/;
use Data::Dump qw/pp/;
use Path::Tiny;
use JSON;
use Tie::IxHash;
use Bean::TemplateWrangler::Storage::Directory::Directory;
use Bean::TemplateWrangler::Storage::Directory::LaxParsing;
use Mojo::Parameters;
use Log::Any qw($log);
$Bean::TemplateWrangler::Storage::Directory::LaxParsing::ALLOW_LAX_TEMPLATES = 1;


my %full_template;
my $folder_path = '/Users/ymaalikjaleel/dev/backend/';
my %test;

# pp  $test{body};
sub run {
  my $self = shift;
  %full_template = ();

   my $template_name = $self->param('template');
  # my $template_name = $ARGV[0];
  find_template($template_name);
  my $feed_type = $full_template{'meta'}{feed_type};

  if($full_template{'meta'}{url} =~ m/robots.adcourier.com/){
    $feed_type = "Robot";
  }elsif($full_template{'meta'}{url} =~ m/batch/){
    $feed_type = "Batch";
  }elsif($full_template{'meta'}{url} =~ m/wpjmbb/){
    $feed_type = "Wordpress XML";
  }

  $self->stash(
    mappings      => $full_template{'custom_lists'},
    location      => $full_template{other}->{location},
    name          => $full_template{'name'},
    meta          => $full_template{'meta'},
    feed_type     => $feed_type,
    expected_body => $full_template{'expected_body'},
    full_template => \%test
  );

  $self->render();
  return %full_template;
}


sub find_template {
  my $name = shift; 
  my $directory = Bean::TemplateWrangler::Storage::Directory::Directory->new({
  dir => dir($folder_path.'templates')
  });

  my $template_obj = $directory->template( $name );

  my %descriptors = %{ $template_obj->descriptors };

  for my $key ( sort keys %descriptors ) {
    next if
      $descriptors{ $key }->template->is_default;

      lists($key, %descriptors);
      get_descriptors($key, %descriptors);
      get_expected_body($key, %descriptors);
      location($key, %descriptors);


  }
  $full_template{'name'} = $template_obj->name;
  $full_template{'meta'} = $template_obj->fields;
  return %full_template;
}


sub lists {
  my ($descriptor_name, %descriptors_obj) = @_;

  my $type = $descriptors_obj{ $descriptor_name }->type;
  my $list_args = $descriptors_obj{ $descriptor_name }->type_args;
  my $nice_name = $descriptors_obj{ $descriptor_name }->name;
  my @mapping_check = (
    "MultiList",
    "LinkedList",
    "MLinkedList",
    "MultiListFromFile",
    "ListFromFile",
    "List"
  );
  my %check = map { $_ => 1 } @mapping_check;

  if(exists($check{$type})) { 
    if ($list_args =~ m/.txt/){
       get_mapping_file($list_args,$descriptor_name,$type,$nice_name);
    }else{
      create_list($list_args,$descriptor_name,$type,$nice_name);
    }
  }
  
}

# get all mappings from external files
sub get_mapping_file{
    my ($filename,$descriptor_name,$type,$nice_name) = @_;
    my $mapping_file;
    my %mapping_fromfile;

    #certain mapping files are in different folders
     if($type eq 'MultiListFromFile' || $type eq 'ListFromFile'){
        my $file_path = $folder_path . 'mappings.from_list/' . $filename;
        $mapping_file = path($file_path);
        my @lines = $mapping_file->lines;
        
        #normalise mapping files if there is = or !!
        foreach my $l (@lines){
            $l =~ s/=/!!/;
            if( $l =~ m/!!/){
                my @values = split /!!/, $l;
                chomp $values[1];
                $mapping_fromfile{$values[0]} = $values[1];
            }else{
                chomp $l;
                $mapping_fromfile{$l} = $l;
            }
        }
    
     }elsif($type eq 'MLinkedList' || $type eq 'LinkedList'){
        my $file_path = $folder_path . 'linked_list_data/' . $filename;
        $mapping_file = path($file_path);

        my @lines = $mapping_file->lines;
        
        #normalise mapping files if there is = or !!
        foreach my $l (@lines){
            $l =~ s/=/!!/g;
            if( $l =~ m/!!/){
                my @values = split /,/, $l;
                chomp $values[1];
                my @parent = split /!!/,$values[0];
                my @child = split /!!/,$values[1];
                my $ll_name = $parent[1] ? "$parent[0] = $parent[1]" : $parent[0];
                $mapping_fromfile{$ll_name}->{$child[0]} = $child[1];
            }
        }
     }

  
  $full_template{'custom_lists'}{$descriptor_name}{'mapping'} = {%mapping_fromfile};
  $full_template{'custom_lists'}{$descriptor_name}{'type'} = $type;
  $full_template{'custom_lists'}{$descriptor_name}{'nice_name'} = $nice_name;
  
}


sub create_list {
  my ($list,$descriptor_name,$type,$nice_name,%converted_mapping) = @_;
  tie %converted_mapping, 'Tie::IxHash';

  my @all_list = split(/\|/, $list);

  foreach my $l (@all_list){
    my @item = split /!!/,$l;
    if($item[1]){
      $converted_mapping{$item[0]} = $item[1];
    }else{
      $converted_mapping{$item[0]} = $item[0];
    }
  }

  $full_template{'custom_lists'}{$descriptor_name}{'mapping'} = {%converted_mapping};
  $full_template{'custom_lists'}{$descriptor_name}{'type'} = $type;
  $full_template{'custom_lists'}{$descriptor_name}{'nice_name'} = $nice_name;
}

sub get_descriptors {
  my ($descriptor_name, %descriptors_obj) = @_;
  my $nice_name = $descriptors_obj{ $descriptor_name }->name;
  my $type = $descriptors_obj{ $descriptor_name }->type;

    my @type_check = (
      "AuthToken",
      "Hidden",
      "MISSING",
      "Step3LifeList",
      'MLinkedList'.
      'LinkedList'.
      'MultiListFromFile',
      'ListFromFile',
      'List'

    );
    my %check = map { $_ => 1 } @type_check;

  if ($nice_name && !exists($check{$type})){
    $full_template{'custom_fields'}{$descriptor_name}->{'name'} = $nice_name;
    $full_template{'custom_fields'}{$descriptor_name}->{'type'} = $type;

  }
}

sub get_expected_body {
  my ($descriptor_name, %descriptors_obj) = @_;
  my $helper_metric = $descriptors_obj{ $descriptor_name }->helper_metric;

  if( $helper_metric && $helper_metric eq 300){
   
    my $sub_body = $descriptors_obj{ $descriptor_name }->helper->body;
    my $default_funcitons = "
      sub encode_clean {
          return \@_;
      }
      sub html_breaks {
        return \@_;
      }
      sub isoandhtmlentities{
        return \@_;
      }
      ";
    my $strip_body = strip_post_args($descriptors_obj{ $descriptor_name }->helper_raw);


    my $to_eval = "$default_funcitons my \$sub = $sub_body; \$sub->($strip_body);";
    my $body = eval $to_eval;

    $full_template{'expected_body'}->{$descriptor_name}->{'body'} = $body;
  }
     
}

sub strip_post_args {
  my $helper = shift;
  my $regex = qr/\w+\((.*)\)/;

  $helper =~ s/%//g;

  if ($helper =~ $regex){ 
    $helper = $1;
    my @args = split('\|', $helper);
    my $final = join ',', map { qq{'$_'} } @args;
    return $final;
  }


}


sub location {
  my ($descriptor_name, %descriptors_obj) = @_;
  my $helper = $descriptors_obj{ $descriptor_name }->helper_raw;
  my $location_mapping = qr/location_mapping\((\w+)/;
  my $location_id_mapping = qr/location_id_mapping\((\w+)/;

      if($helper){

        if ($helper =~ m/$location_mapping/){

         $full_template{'other'}{'location'} = $1;

        } 
      }
      
}






1;

