package TemplateBreakdown::Controller::Search;
use Mojo::Base 'Mojolicious::Controller';

# This action will render a template
sub start {
  my $self = shift;

  # Render template "example/welcome.html.ep" with message
  $self->render(msg => 'Template Breakdown');
}

1;
