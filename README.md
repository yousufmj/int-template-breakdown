# README #

Using template wrangler to create a simple front end to view templates

### How do I get set up? ###

•Check out repo

•Have a an svn checkout of backend folder
`svn co https://svn.adcourier.com/adcourier/backend/ ` - put this anywhere you what 

	-Template folder, and all mapping files

•Install Template Wrangler Module using pinto
 
 •Refer to track/wiki/pinto to install pinto
 
`cpanm ' https://broadbean:PASSWORD@aws_pinto.adcourier.com/' Bean::TemplateWrangler`

•change location of backend folder
	- ` lib / TemplateBreakdown / Controller / Reader.pm` - on line 15
`/Users/ymaalikjaleel/dev/backend/`

*run the app
`morbo script/template_breakdown`

•view all templates
	`http://127.0.0.1:3000/break_down?template=TEMPLATE_NAME`



### any issues or queries? ###

•Speak to Yousuf 